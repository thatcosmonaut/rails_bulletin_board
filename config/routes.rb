RailsBulletinBoard::Engine.routes.draw do
  devise_for :users, :class_name => "RailsBulletinBoard::User", module: :devise

  resources :posts, except: [:show]
  resources :threads, except: [:update]
  resources :forums
end
