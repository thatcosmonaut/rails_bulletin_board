module RailsBulletinBoard
  class ForumAuthorizer < ApplicationAuthorizer
    def creatable_by?(user)
      user.has_role?(:admin)
    end

    def readable_by?(user)
      if resource.private
        user.has_role?(:admin) ||
          user.has_role?(:moderator, resource)
      else
        true
      end
    end

    def updatable_by?(user)
      user.has_role?(:admin)
    end

    def deletable_by?(user)
      user.has_role?(:admin)
    end
  end
end
