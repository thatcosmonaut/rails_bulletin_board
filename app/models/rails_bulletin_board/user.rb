module RailsBulletinBoard
  class User < ActiveRecord::Base
    include Authority::UserAbilities

    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable

    has_many :threads, foreign_key: :author_id
    has_many :posts


    rolify role_cname: "RailsBulletinBoard::Role"
  end
end
