class RenameColumnsInUserRoles < ActiveRecord::Migration
  def change
    rename_column :rails_bulletin_board_users_rails_bulletin_board_roles, :rails_bulletin_board_user_id, :user_id
    rename_column :rails_bulletin_board_users_rails_bulletin_board_roles, :rails_bulletin_board_role_id, :role_id

  end
end
