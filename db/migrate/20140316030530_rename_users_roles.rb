class RenameUsersRoles < ActiveRecord::Migration
  def change
    rename_table :users_roles, :rails_bulletin_board_users_rails_bulletin_board_roles
  end
end
