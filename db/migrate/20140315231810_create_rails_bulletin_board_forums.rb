class CreateRailsBulletinBoardForums < ActiveRecord::Migration
  def change
    create_table :rails_bulletin_board_forums do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
