class AddContentToPost < ActiveRecord::Migration
  def change
    add_column :rails_bulletin_board_posts, :content, :text
  end
end
