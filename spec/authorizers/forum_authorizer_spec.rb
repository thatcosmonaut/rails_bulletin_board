require 'spec_helper'

describe RailsBulletinBoard::ForumAuthorizer do

  context 'create' do
    let(:forum) { build(:rails_bulletin_board_forum) }

    context 'user is an admin' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:admin)
        user
      end

      it 'can be created by user' do
        expect(forum.creatable_by?(user)).to eql true
      end
    end

    context 'user is not an admin' do
      let(:user) { create(:rails_bulletin_board_user) }

      it 'cannot be created by user' do
        expect(forum.creatable_by?(user)).to eql false
      end
    end
  end

  context 'read' do

    context 'forum is private' do
      let(:forum) { create(:rails_bulletin_board_forum, private: true) }

      context 'user is an admin' do
        let(:user) do
          user = create(:rails_bulletin_board_user)
          user.add_role(:admin)
          user
        end

        it 'can be read by user' do
          expect(forum.readable_by?(user)).to eql true
        end
      end

      context 'user is a moderator on the forum' do
        let(:user) do
          user = create(:rails_bulletin_board_user)
          user.add_role(:moderator, forum)
          user
        end

        it 'can be read by user' do
          expect(forum.readable_by?(user)).to eql true
        end
      end

      context 'user is not an admin or member of the forum' do
        let(:user) { create(:rails_bulletin_board_user) }

        it 'cannot be read by user' do
          expect(forum.readable_by?(user)).to eql false
        end
      end

    end
  end

  context 'update' do
    let(:forum) { create(:rails_bulletin_board_forum) }

    context 'user is an admin' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:admin)
        user
      end

      it 'can be updated by user' do
        expect(forum.updatable_by?(user)).to eql true
      end
    end

    context 'user is not an admin' do
      let(:user) { create(:rails_bulletin_board_user) }

      it 'cannot be updated by user' do
        expect(forum.updatable_by?(user)).to eql false
      end
    end
  end

  context 'delete' do
    let(:forum) { create(:rails_bulletin_board_forum) }

    context 'user is an admin' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:admin)
        user
      end

      it 'can be deleted by user' do
        expect(forum.deletable_by?(user)).to eql true
      end
    end

    context 'user is not an admin' do
      let(:user) { create(:rails_bulletin_board_user) }

      it 'cannot be deleted by user' do
        expect(forum.deletable_by?(user)).to eql false
      end
    end
  end

end
