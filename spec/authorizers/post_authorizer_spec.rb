require 'spec_helper'

describe RailsBulletinBoard::PostAuthorizer do

  context 'create' do
    let(:post) { build(:rails_bulletin_board_post) }

    context 'user is an admin' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:admin)
        user
      end

      it 'can be created by user' do
        expect(post.creatable_by?(user)).to eql true
      end
    end

    context 'user is a moderator on the forum' do
      let(:forum) { create(:rails_bulletin_board_forum) }
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:moderator, post.thread.forum)
        user
      end

      context 'thread is locked' do
        let(:thread) { create(:rails_bulletin_board_thread, locked: true) }
        let(:post) { build(:rails_bulletin_board_post, thread: thread) }

        it 'can be created by user' do
          expect(post.creatable_by?(user)).to eql true
        end
      end

      context 'forum is private' do
        let(:forum) { create(:rails_bulletin_board_forum, private: true) }
        let(:thread) { create(:rails_bulletin_board_thread, forum: forum) }
        let(:post) { build(:rails_bulletin_board_post, thread: thread) }

        it 'can be created by user' do
          expect(post.creatable_by?(user)).to eql true
        end
      end
    end

    context 'user is a moderator on a different forum' do
      let(:forum) { create(:rails_bulletin_board_forum) }
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:moderator, create(:rails_bulletin_board_forum))
        user
      end

      context 'thread is locked' do
        let(:thread) { create(:rails_bulletin_board_thread, forum: forum, locked: true) }
        let(:post) { build(:rails_bulletin_board_post, thread: thread) }

        it 'cannot be created by user' do
          expect(post.creatable_by?(user)).to eql false
        end
      end

      context 'forum is private' do
        let(:forum) { create(:rails_bulletin_board_forum, private: true) }
        let(:thread) { create(:rails_bulletin_board_thread, forum: forum) }
        let(:post) { build(:rails_bulletin_board_post, thread: thread) }

        it 'cannot be created by user' do
          expect(post.creatable_by?(user)).to eql false
        end
      end
    end

    context 'user has no special permissions' do
      let(:user) { create(:rails_bulletin_board_user) }

      context 'forum is private' do
        let(:forum) { create(:rails_bulletin_board_forum, private: true) }
        let(:thread) { create(:rails_bulletin_board_thread, forum: forum) }
        let(:post) { build(:rails_bulletin_board_post, thread: thread) }

        it 'cannot be created by user' do
          expect(post.creatable_by?(user)).to eql false
        end
      end

      context 'thread is locked' do
        let(:thread) { create(:rails_bulletin_board_thread, locked: true) }
        let(:post) { build(:rails_bulletin_board_post, thread: thread) }

        it 'cannot be created by user' do
          expect(post.creatable_by?(user)).to eql false
        end
      end

      context 'thread is not locked and forum is not private' do
        it 'can be created by user' do
          expect(post.creatable_by?(user)).to eql true
        end
      end
    end
  end

  context 'update' do
    let(:forum) { create(:rails_bulletin_board_forum) }
    let(:thread) { create(:rails_bulletin_board_thread, forum: forum) }
    let(:post) { create(:rails_bulletin_board_post, thread: thread) }

    context 'user is an admin' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:admin)
        user
      end

      it 'can be updated by user' do
        expect(post.updatable_by?(user)).to eql true
      end
    end

    context 'user is a moderator on the forum' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:moderator, forum)
        user
      end

      it 'can be updated by user' do
        expect(post.updatable_by?(user)).to eql true
      end
    end

    context 'user has no special permissions' do
      let(:user) { create(:rails_bulletin_board_user) }

      context 'user made post' do
        let(:post) { create(:rails_bulletin_board_post, user: user) }

        it 'can be updated by user' do
          expect(post.updatable_by?(user)).to eql true
        end
      end

      context 'user did not make post' do
        let(:post) { create(:rails_bulletin_board_post) }

        it 'cannot be updated by user' do
          expect(post.updatable_by?(user)).to eql false
        end
      end
    end
  end

  context 'delete' do
    let(:forum) { create(:rails_bulletin_board_forum) }
    let(:thread) { create(:rails_bulletin_board_thread, forum: forum) }
    let(:post) { create(:rails_bulletin_board_post, thread: thread) }

    context 'user is an admin' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:admin)
        user
      end

      it 'can be deleted by user' do
        expect(post.deletable_by?(user)).to eql true
      end
    end

    context 'user is a moderator on the forum' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:moderator, forum)
        user
      end

      it 'can be deleted by user' do
        expect(post.deletable_by?(user)).to eql true
      end
    end

    context 'user has no special permissions' do
      let(:user) { create(:rails_bulletin_board_user) }

      context 'user made post' do
        let(:post) { create(:rails_bulletin_board_post, user: user) }

        it 'can be deleted by user' do
          expect(post.deletable_by?(user)).to eql true
        end
      end

      context 'user did not make post' do
        it 'cannot be deleted by user' do
          expect(post.deletable_by?(user)).to eql false
        end
      end
    end
  end

end
