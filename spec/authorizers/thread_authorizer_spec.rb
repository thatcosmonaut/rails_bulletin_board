require 'spec_helper'

describe RailsBulletinBoard::ThreadAuthorizer do

  context 'create' do
    let(:thread) { build(:rails_bulletin_board_thread) }

    context 'user is an admin' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:admin)
        user
      end

      it 'can be created by user' do
        expect(thread.creatable_by?(user)).to eql true
      end
    end

    context 'user is a moderator on the forum' do
      let(:forum) { create(:rails_bulletin_board_forum) }
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:moderator, forum)
        user
      end

      it 'can be created by user' do
        expect(thread.creatable_by?(user)).to eql true
      end
    end

    context 'user has no special permissions' do
      let(:user) { create(:rails_bulletin_board_user) }

      context 'forum is private' do
        let(:forum) { create(:rails_bulletin_board_forum, private: true) }
        let(:thread) { create(:rails_bulletin_board_thread, forum: forum) }

        it 'cannot be created by user' do
          expect(thread.creatable_by?(user)).to eql false
        end
      end

      context 'forum is locked' do
        let(:forum) { create(:rails_bulletin_board_forum, locked: true) }
        let(:thread) { create(:rails_bulletin_board_thread, forum: forum) }

        it 'cannot be created by user' do
          expect(thread.creatable_by?(user)).to eql false
        end
      end

      context 'forum has no special conditions' do
        it 'can be created by user' do
          expect(thread.creatable_by?(user)).to eql true
        end
      end
    end

  end

  context 'read' do
    let(:forum) { create(:rails_bulletin_board_forum) }
    let(:thread) { create(:rails_bulletin_board_thread, forum: forum) }

    context 'user is an admin' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:admin)
        user
      end

      it 'can be read by user' do
        expect(thread.readable_by?(user)).to eql true
      end
    end

    context 'user is a moderator on the forum' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:moderator, forum)
        user
      end

      it 'can be read by user' do
        expect(thread.readable_by?(user)).to eql true
      end
    end

    context 'user has no special permissions' do
      let(:user) { create(:rails_bulletin_board_user) }

      context 'forum is private' do
        let(:forum) { create(:rails_bulletin_board_forum, private: true) }

        it 'cannot be read by user' do
          expect(thread.readable_by?(user)).to eql false
        end
      end

      context 'forum has no special conditions' do
        it 'can be read by user' do
          expect(thread.readable_by?(user)).to eql true
        end
      end
    end

  end

  context 'delete' do
    let(:thread) { create(:rails_bulletin_board_thread) }

    context 'user is an admin' do
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:admin)
        user
      end

      it 'can be deleted by user' do
        expect(thread.deletable_by?(user)).to eql true
      end
    end

    context 'user is a moderator on the forum' do
      let(:forum) { create(:rails_bulletin_board_forum) }
      let(:thread) { create(:rails_bulletin_board_thread, forum: forum) }
      let(:user) do
        user = create(:rails_bulletin_board_user)
        user.add_role(:moderator, forum)
        user
      end

      it 'can be deleted by user' do
        expect(thread.deletable_by?(user)).to eql true
      end
    end

    context 'user has no special permissions' do
      let(:user) { create(:rails_bulletin_board_user) }

      it 'cannot be deleted by user' do
        expect(thread.deletable_by?(user)).to eql false
      end
    end
  end
end
