# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :rails_bulletin_board_user, class: RailsBulletinBoard::User do
    email 'abc@test.com'
    password 'blehblah'
  end
end
