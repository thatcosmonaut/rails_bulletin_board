module AuthenticationHelper
  def sign_in(user)
    page.driver.submit :delete, destroy_user_session_path, {}
    visit new_user_session_path
    find(:css, "#user_email").set(user.email)
    find(:css, "#user_password").set(user.password)
    click_button 'Sign In'
    user
  end
end
