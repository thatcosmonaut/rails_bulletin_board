require 'spec_helper'

module RailsBulletinBoard
  describe ForumController do
    let(:forum) { create(:rails_bulletin_board_forum) }
    let(:user) { create(:rails_bulletin_board_user) }

    before do
      sign_in user
    end

    context 'user views forum' do
      it 'returns 200' do
        get :show, id: forum.id, use_route: :forums
        assert_response :ok
      end
    end
  end
end
